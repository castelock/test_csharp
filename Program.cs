﻿// See https://aka.ms/new-console-template for more information
Punto punto1;
punto1 = new Punto(4, 7);
// punto1 = new Punto(punto1.X, 2); // Modificación
Punto punto2 = new Punto(4, 7);

if(punto1.Equals(punto2)){
    Console.WriteLine("They are the same");
}
else
    Console.WriteLine("NOT the same");


public readonly struct Punto
{
public int X { get; init; }
public int Y { get; init; }
public Punto(int valorX, int valorY)
    {
        X = valorX;
        Y = valorY;
    }
}
